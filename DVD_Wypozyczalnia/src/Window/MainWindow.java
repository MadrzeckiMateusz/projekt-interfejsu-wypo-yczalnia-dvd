package Window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.freixas.jcalendar.JCalendarCombo;

import java.awt.ComponentOrientation;
import java.awt.Rectangle;

import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainWindow extends JFrame{
	private JPanel panel;
	private JPanel Status;
	private JTabbedPane tabbedPane;
	private JPanel panel_1;
	private JPanel panel_3;
	private JPanel panel_2;
	private JPanel panel_4;
	private JPanel panel_5;
	private JMenuBar menuBar;
	private JPanel panel_6;
	private JPanel panel_8;
	private JScrollPane scrollPane;
	private JPanel panel_7;
	private JTable table;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JComboBox comboBox_1;
	private JComboBox comboBox_2;
	private JComboBox comboBox_4;
	private JButton ToolBarBtnRemove;
	private JButton ToolBarBtnNewAdd;
	private JButton ToolBarBtnEdit;
	private JButton ToolBarBtnDetails;
	private JButton ToolBarBtnPrint;
	private JButton ToolBarBtnSave;
	private JLabel lblNewLabel_8;
	private JSeparator separator_3;
	private JLabel lblNumLock;
	private JSeparator separator_4;
	private JLabel Datalabel;
	private Date data;
	private JSeparator separator_2;
	private JSeparator separator_5;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JMenuItem mntmNewMenuItem_4;
	private JSeparator separator_6;
	private JSeparator separator_7;
	private JMenuItem mntmNewMenuItem_5;
	private JMenuItem mntmNewMenuItem_6;
	private JMenuItem mntmNewMenuItem_7;
	private JMenuItem mntmNewMenuItem_8;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem_9;
	private JMenuItem mntmNewMenuItem_10;
	private JMenuItem mntmNewMenuItem_11;
	private JSeparator separator_8;
	private JMenuItem mntmNewMenuItem_12;
	public MainWindow() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/Resources/icons/DVD48.png")));
		setMinimumSize(new Dimension(900, 600));
		setTitle("DVD Support");
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		data = new Date();
		menuBar = new JMenuBar();
		menuBar.setBorderPainted(false);
		menuBar.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		setJMenuBar(menuBar);
		
		JMenu mnPlik = new JMenu("Plik");
		menuBar.add(mnPlik);
		
		mntmNewMenuItem_2 = new JMenuItem("Zapisz");
		mnPlik.add(mntmNewMenuItem_2);
		
		mntmNewMenuItem_1 = new JMenuItem("Zapisz jako");
		mnPlik.add(mntmNewMenuItem_1);
		
		separator_7 = new JSeparator();
		mnPlik.add(separator_7);
		
		mntmNewMenuItem_3 = new JMenuItem("Drukuj");
		mnPlik.add(mntmNewMenuItem_3);
		
		separator_6 = new JSeparator();
		mnPlik.add(separator_6);
		
		mntmNewMenuItem_4 = new JMenuItem("Wyloguj");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LoginWindow().setVisible(true);
			}
		});
		mnPlik.add(mntmNewMenuItem_4);
		
		mntmNewMenuItem = new JMenuItem("Zamknij");
		mnPlik.add(mntmNewMenuItem);
		
		JMenu mnKlient = new JMenu("Klient");
		menuBar.add(mnKlient);
		
		mntmNewMenuItem_5 = new JMenuItem("Dodaj");
		mnKlient.add(mntmNewMenuItem_5);
		
		mntmNewMenuItem_6 = new JMenuItem("Edytuj");
		mnKlient.add(mntmNewMenuItem_6);
		
		mntmNewMenuItem_7 = new JMenuItem("Usuń");
		mnKlient.add(mntmNewMenuItem_7);
		
		mntmNewMenuItem_8 = new JMenuItem("Znajdź");
		mnKlient.add(mntmNewMenuItem_8);
		
		JMenu mnFilmy = new JMenu("Filmy");
		menuBar.add(mnFilmy);
		
		JMenu mnWypoyczenia = new JMenu("Wypożyczenia");
		menuBar.add(mnWypoyczenia);
		
		JMenu mnRaporty = new JMenu("Raporty");
		menuBar.add(mnRaporty);
		
		mnNewMenu = new JMenu("Generuj");
		mnRaporty.add(mnNewMenu);
		
		mntmNewMenuItem_9 = new JMenuItem("Raport Wypożyczeń");
		mnNewMenu.add(mntmNewMenuItem_9);
		
		mntmNewMenuItem_10 = new JMenuItem("Raport Wydajności Pracowników");
		mnNewMenu.add(mntmNewMenuItem_10);
		
		mntmNewMenuItem_11 = new JMenuItem("Raport Klientów");
		mnNewMenu.add(mntmNewMenuItem_11);
		
		separator_8 = new JSeparator();
		mnRaporty.add(separator_8);
		
		mntmNewMenuItem_12 = new JMenuItem("Wyszukaj");
		mnRaporty.add(mntmNewMenuItem_12);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		Status = new JPanel();
		Status.setBackground(SystemColor.control);
		Status.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		Status.setPreferredSize(new Dimension(10, 30));
		Status.setMinimumSize(new Dimension(10, 30));
		panel.add(Status, BorderLayout.SOUTH);
		Status.setLayout(null);
		
		JLabel lblNewLabel_6 = new JLabel("Info ");
		lblNewLabel_6.setBounds(10, 5, 23, 20);
		Status.add(lblNewLabel_6);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(605, 0, 2, 30);
		Status.add(separator_1);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblNewLabel_7.setBounds(40, 5, 555, 20);
		Status.add(lblNewLabel_7);
		
		lblNewLabel_8 = new JLabel("Caps Lcok");
		lblNewLabel_8.setForeground(Color.GRAY);
		lblNewLabel_8.setBounds(617, 5, 53, 20);
		Status.add(lblNewLabel_8);
		
		separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(680, 0, 2, 30);
		Status.add(separator_3);
		
		lblNumLock = new JLabel("Num Lock");
		lblNumLock.setForeground(Color.GRAY);
		lblNumLock.setBounds(700, 5, 53, 20);
		Status.add(lblNumLock);
		
		separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setBounds(763, 0, 2, 30);
		Status.add(separator_4);
		
		Datalabel = new JLabel("");
		Datalabel.setHorizontalAlignment(SwingConstants.CENTER);
		Datalabel.setBounds(775, 5, 99, 20);
		Status.add(Datalabel);
		Datalabel.setText("2014-05-15  20:08");
		
		tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setBackground(UIManager.getColor("Panel.background"));
		tabbedPane.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		panel.add(tabbedPane, BorderLayout.CENTER);
		
		panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		tabbedPane.addTab(null,  panel_1);
		JLabel label1 = new JLabel("Wypożyczenia", new ImageIcon(MainWindow.class.getResource("/Resources/icons/wypo32.png")), JLabel.LEFT);
		label1.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(0, label1);
		tabbedPane.setForegroundAt(0, Color.WHITE);
		panel_1.setLayout(new BorderLayout(2, 2));
		
		scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setViewportBorder(null);
		scrollPane.setBorder(null);
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"WYP01\\02\\55", "KL01\\02\\14\\001", "MOV01\\001", "2014-04-23", "Zrealizowane", null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Nr", "Nr klienta", "Nr filmu", "Data", "Status", "Termin zwrotu"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(90);
		table.getColumnModel().getColumn(0).setMinWidth(90);
		table.getColumnModel().getColumn(1).setPreferredWidth(90);
		table.getColumnModel().getColumn(1).setMinWidth(90);
		table.getColumnModel().getColumn(2).setPreferredWidth(90);
		table.getColumnModel().getColumn(2).setMinWidth(90);
		table.getColumnModel().getColumn(3).setPreferredWidth(90);
		table.getColumnModel().getColumn(3).setMinWidth(90);
		table.getColumnModel().getColumn(4).setPreferredWidth(90);
		table.getColumnModel().getColumn(4).setMinWidth(90);
		table.getColumnModel().getColumn(5).setPreferredWidth(90);
		table.getColumnModel().getColumn(5).setMinWidth(90);
		scrollPane.setViewportView(table);
		
		panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel_7.setBorder(new TitledBorder(BorderFactory.createLineBorder(new Color(0,0,0)), "Wyszukiwanie", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_7.setPreferredSize(new Dimension(210, 10));
		panel_7.setMinimumSize(new Dimension(210, 10));
		panel_1.add(panel_7, BorderLayout.EAST);
		panel_7.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nr");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 26, 90, 14);
		panel_7.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nr klienta");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 51, 90, 14);
		panel_7.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Nr filmu");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(10, 76, 90, 14);
		panel_7.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Data");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(10, 101, 90, 14);
		panel_7.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Status");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_4.setBounds(10, 126, 90, 14);
		panel_7.add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("Termin zwrotu");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_5.setBounds(10, 151, 90, 14);
		panel_7.add(lblNewLabel_5);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBackground(Color.LIGHT_GRAY);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "WYP01\\02\\55", "WYP01\\02\\56", "WYP01\\02\\57"}));
		comboBox.setBounds(103, 23, 97, 20);
		panel_7.add(comboBox);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setEditable(true);
		comboBox_1.setBounds(103, 48, 97, 20);
		panel_7.add(comboBox_1);
		
		comboBox_2 = new JComboBox();
		comboBox_2.setEditable(true);
		comboBox_2.setBounds(103, 73, 97, 20);
		panel_7.add(comboBox_2);
		
		comboBox_4 = new JComboBox();
		comboBox_4.setEditable(true);
		comboBox_4.setBounds(103, 123, 97, 20);
		panel_7.add(comboBox_4);
		
		JCalendarCombo calendarCombo = new JCalendarCombo();
		calendarCombo.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		calendarCombo.setBounds(103, 98, 97, 20);
		panel_7.add(calendarCombo);
		
		JCalendarCombo calendarCombo_1 = new JCalendarCombo();
		calendarCombo_1.setBounds(103, 148, 97, 20);
		calendarCombo_1.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		panel_7.add(calendarCombo_1);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.BLACK);
		separator.setBounds(4, 183, 202, 1);
		panel_7.add(separator);
		
		JButton btnNewButton = new JButton("Wyszukaj");
		btnNewButton.setBounds(103, 197, 97, 20);
		panel_7.add(btnNewButton);
		tabbedPane.setBackgroundAt(0, Color.BLACK);
		
		panel_6 = new JPanel();
		
		tabbedPane.addTab( null,panel_6);
		JLabel label2 = new JLabel("Rezerwacje     ", new ImageIcon(MainWindow.class.getResource("/Resources/icons/orders32.png")),JLabel.LEFT);
		label2.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(1, label2);
		
		panel_3 = new JPanel();
		tabbedPane.addTab(null,  panel_3);
		JLabel label3 = new JLabel("Filmy               ", new ImageIcon(MainWindow.class.getResource("/Resources/icons/movie32.png")),JLabel.LEFT);
		label3.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(2, label3);
		
		panel_2 = new JPanel();
		tabbedPane.addTab(null, panel_2);
		JLabel label4 = new JLabel("Klienci             ", new ImageIcon(MainWindow.class.getResource("/Resources/icons/klient32.png")),JLabel.LEFT);
		label4.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(3, label4);
		
		panel_5 = new JPanel();
		tabbedPane.addTab(null, panel_5);
		JLabel label5 = new JLabel("Pracownicy     ", new ImageIcon(MainWindow.class.getResource("/Resources/icons/employ32.png")),JLabel.LEFT);
		label5.setHorizontalTextPosition(JLabel.LEFT);
		
		tabbedPane.setTabComponentAt(4, label5);
		
		panel_4 = new JPanel();
		tabbedPane.addTab(null, panel_4);
		JLabel label6 = new JLabel("Statystyka      ",  new ImageIcon(MainWindow.class.getResource("/Resources/icons/Statistic32.png")),JLabel.LEFT);
		label6.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(5, label6);
		
		panel_8 = new JPanel();
		tabbedPane.addTab(null, panel_8);
		JLabel label7 = new JLabel("Cennik             ",  new ImageIcon(MainWindow.class.getResource("/Resources/icons/pricing.png")),JLabel.LEFT);
		label7.setHorizontalTextPosition(JLabel.LEFT);
		tabbedPane.setTabComponentAt(6, label7);
		label5.setVisible(false);
		label6.setVisible(false);
		label7.setVisible(false);
		tabbedPane.setEnabledAt(4, false);
		tabbedPane.setEnabledAt(5, false);
		tabbedPane.setEnabledAt(6, false);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setLayout(null);
		toolBar.setMargin(new Insets(10, 20, 10, 20));
		toolBar.setLocation(new Point(50, 0));
		toolBar.setAlignmentX(5.0f);
		toolBar.setPreferredSize(new Dimension(13, 40));
		toolBar.setMaximumSize(new Dimension(13, 35));
		panel.add(toolBar, BorderLayout.NORTH);
		
		ToolBarBtnDetails = new JButton("");
		ToolBarBtnDetails.setBounds(new Rectangle(10, 5, 30, 30));
		ToolBarBtnDetails.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/more24.png")));
		ToolBarBtnDetails.setPreferredSize(new Dimension(30, 30));
		ToolBarBtnDetails.setMinimumSize(new Dimension(30, 30));
		ToolBarBtnDetails.setMaximumSize(new Dimension(30, 30));
		toolBar.add(ToolBarBtnDetails);
		
		ToolBarBtnNewAdd = new JButton("");
		ToolBarBtnNewAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AddDataView();
			}
		});
		ToolBarBtnNewAdd.setBounds(new Rectangle(55, 5, 30, 30));
		ToolBarBtnNewAdd.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/add24.png")));
		ToolBarBtnNewAdd.setMaximumSize(new Dimension(30, 30));
		ToolBarBtnNewAdd.setMinimumSize(new Dimension(30, 30));
		ToolBarBtnNewAdd.setPreferredSize(new Dimension(30, 30));
		toolBar.add(ToolBarBtnNewAdd);
		
		ToolBarBtnRemove = new JButton("");
		ToolBarBtnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//JOptionPane.showMessageDialog(MainWindow.this, "Rekord usunięty pomyślnie!!!", "Informacja",JOptionPane.INFORMATION_MESSAGE,new ImageIcon(MainWindow.class.getResource("/Resources/icons/info32.png")));
		//		JOptionPane.showMessageDialog(MainWindow.this, "Błąd aktualizacji danych w tabeli Zamówienia !!!", "Błąd",JOptionPane.ERROR_MESSAGE,new ImageIcon(MainWindow.class.getResource("/Resources/icons/error32.png")));
				//JOptionPane.showConfirmDialog(MainWindow.this,  "Pytanie",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,new ImageIcon(MainWindow.class.getResource("/Resources/icons/questionn32.png")));
				Object[] options = {"Tak",
	                    "Nie"};
	int n = JOptionPane.showOptionDialog(MainWindow.this,
			"Czy napewno usunąć rekord z tabeli Pracownicy ???",
	    "Pytanie",
	    JOptionPane.YES_NO_OPTION,
	    JOptionPane.QUESTION_MESSAGE,
	    new ImageIcon(MainWindow.class.getResource("/Resources/icons/questionn32.png")),
	    options,
	    options[1]);

			}
		});
		ToolBarBtnRemove.setBounds(new Rectangle(85, 5, 30, 30));
		ToolBarBtnRemove.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/delete24.png")));
		ToolBarBtnRemove.setMaximumSize(new Dimension(30, 30));
		ToolBarBtnRemove.setPreferredSize(new Dimension(30, 30));
		toolBar.add(ToolBarBtnRemove);
		
		ToolBarBtnEdit = new JButton("");
		ToolBarBtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new EditDataView();
			}
		});
		ToolBarBtnEdit.setBounds(new Rectangle(115, 5, 30, 30));
		ToolBarBtnEdit.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/edit24.png")));
		ToolBarBtnEdit.setPreferredSize(new Dimension(30, 30));
		ToolBarBtnEdit.setMinimumSize(new Dimension(30, 30));
		ToolBarBtnEdit.setMaximumSize(new Dimension(30, 30));
		toolBar.add(ToolBarBtnEdit);
		
		separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(new Rectangle(48, 0, 2, 40));
		separator_2.setMaximumSize(new Dimension(2, 40));
		toolBar.add(separator_2);
		
		separator_5 = new JSeparator();
		separator_5.setOrientation(SwingConstants.VERTICAL);
		separator_5.setMaximumSize(new Dimension(2, 40));
		separator_5.setBounds(new Rectangle(148, 0, 2, 40));
		toolBar.add(separator_5);
		
		ToolBarBtnPrint = new JButton("");
		ToolBarBtnPrint.setBounds(new Rectangle(155, 5, 30, 30));
		ToolBarBtnPrint.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/print24.png")));
		ToolBarBtnPrint.setPreferredSize(new Dimension(30, 30));
		ToolBarBtnPrint.setMinimumSize(new Dimension(30, 30));
		ToolBarBtnPrint.setMaximumSize(new Dimension(30, 30));
		toolBar.add(ToolBarBtnPrint);
		
		ToolBarBtnSave = new JButton("");
		ToolBarBtnSave.setBounds(new Rectangle(185, 5, 30, 30));
		ToolBarBtnSave.setPreferredSize(new Dimension(30, 30));
		ToolBarBtnSave.setMaximumSize(new Dimension(30, 30));
		ToolBarBtnSave.setMinimumSize(new Dimension(30, 30));
		ToolBarBtnSave.setIcon(new ImageIcon(MainWindow.class.getResource("/Resources/icons/save.png")));
		toolBar.add(ToolBarBtnSave);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{getContentPane(), panel, Status, tabbedPane, panel_1, scrollPane, table, panel_7, lblNewLabel, lblNewLabel_1, lblNewLabel_2, lblNewLabel_3, lblNewLabel_4, lblNewLabel_5, comboBox, comboBox_1, comboBox_2, comboBox_4, calendarCombo, calendarCombo_1, separator, btnNewButton, panel_6, panel_3, panel_2, panel_5, panel_4, panel_8, menuBar, mnPlik, mnKlient, mnFilmy, mnWypoyczenia, mnRaporty}));
		
	}
}
