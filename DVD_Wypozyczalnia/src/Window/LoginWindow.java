package Window;

import java.awt.Dimension;
import java.awt.Toolkit;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.ImageIcon;

public class LoginWindow extends JFrame {
	public JLabel lblUytkownik;
	public JPasswordField passwordField;
	public JButton btnNewButton;
	public JButton btnNewButton_1;
	public JLabel lblNewLabel;
	public JComboBox comboBox;
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	public LoginWindow() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(LoginWindow.class.getResource("/Resources/icons/DVD48.png")));
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setMinimumSize(new Dimension(327, 174));
		setPreferredSize(new Dimension(327, 174));
		CenterFrame();
		setTitle("Logowanie");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblUytkownik = new JLabel("U\u017Cytkownik ");
		lblUytkownik.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUytkownik.setBounds(98, 23, 67, 20);
		getContentPane().add(lblUytkownik);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBounds(171, 23, 129, 20);
		getContentPane().add(comboBox);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(171, 54, 129, 20);
		getContentPane().add(passwordField);
		
		lblNewLabel = new JLabel("Has\u0142o");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(98, 54, 67, 20);
		getContentPane().add(lblNewLabel);
		
		btnNewButton = new JButton("Zamknij");
		btnNewButton.setIcon(new ImageIcon(LoginWindow.class.getResource("/Resources/icons/delete16.png")));
		btnNewButton.setBounds(210, 96, 89, 23);
		getContentPane().add(btnNewButton);
		
		btnNewButton_1 = new JButton("Zaloguj");
		btnNewButton_1.setIcon(new ImageIcon(LoginWindow.class.getResource("/Resources/icons/login16.png")));
		btnNewButton_1.setBounds(107, 96, 89, 23);
		getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(LoginWindow.class.getResource("/Resources/icons/Authentication-Lock-icon.png")));
		lblNewLabel_1.setBounds(23, 37, 48, 48);
		getContentPane().add(lblNewLabel_1);
	}
	
	 public void CloseWindow()
		{
			int a = JOptionPane.showConfirmDialog(this, "Czy zakończyć program ?",
					"Kończenie pracy programu",JOptionPane.OK_CANCEL_OPTION );
					if (a == JOptionPane.OK_OPTION )
			{
			
				System.exit(0);
			}
			else
				{
					return;
				}
		}
	 public void CenterFrame(){
		 this.DimensionOfFrame = getSize();		
			this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
			if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
				DimensionOfFrame.height = DimensionOfScreenSize.height;
			}
			if (DimensionOfFrame.width > DimensionOfScreenSize.width){
				DimensionOfFrame.width = DimensionOfScreenSize.width;
			}
			
			setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
				      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
	 }
}
