package Window;



import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

public class EditDataView extends JDialog {
	public JPanel panel;
	public JButton Cancel;
	public JButton add;
	public EditDataView() {
		setTitle("Edycja");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AddDataView.class.getResource("/Resources/icons/klient32.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(400, 400));
		setPreferredSize(new Dimension(400, 400));
		setVisible(true);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		add = new JButton("Akceptuj");
		add.setIcon(new ImageIcon(AddDataView.class.getResource("/Resources/icons/yes16.png")));
		add.setBounds(212, 327, 102, 23);
		panel.add(add);
		
		Cancel = new JButton("Anuluj");
		Cancel.setIcon(new ImageIcon(AddDataView.class.getResource("/Resources/icons/no16.png")));
		Cancel.setBounds(70, 327, 102, 23);
		panel.add(Cancel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 314, 384, 2);
		panel.add(separator);
		
		JLabel lblNewLabel = new JLabel("Edytuj Klienta");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(131, 13, 120, 20);
		panel.add(lblNewLabel);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 46, 384, 2);
		panel.add(separator_1);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(AddDataView.class.getResource("/Resources/icons/klient32.png")));
		lblNewLabel_1.setBounds(60, 8, 32, 32);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Numer Klienta");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(90, 65, 100, 20);
		panel.add(lblNewLabel_2);
		
		JLabel lblNazwisko = new JLabel("Nazwisko");
		lblNazwisko.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNazwisko.setBounds(90, 96, 100, 20);
		panel.add(lblNazwisko);
		
		JLabel lblImi = new JLabel("Imię");
		lblImi.setHorizontalAlignment(SwingConstants.RIGHT);
		lblImi.setBounds(90, 127, 100, 20);
		panel.add(lblImi);
		
		JLabel lblNumerPesel = new JLabel("Numer PESEL");
		lblNumerPesel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNumerPesel.setBounds(90, 158, 100, 20);
		panel.add(lblNumerPesel);
		
		JLabel lblAdresKorespondencyjny = new JLabel("Adres Korespondencyjny");
		lblAdresKorespondencyjny.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAdresKorespondencyjny.setBounds(70, 189, 120, 20);
		panel.add(lblAdresKorespondencyjny);
		
		JLabel lblNumerTelefonu = new JLabel("Numer telefonu");
		lblNumerTelefonu.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNumerTelefonu.setBounds(90, 220, 100, 20);
		panel.add(lblNumerTelefonu);
		
		JLabel lblAdresEmail = new JLabel("Adres e-mail");
		lblAdresEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAdresEmail.setBounds(90, 251, 100, 20);
		panel.add(lblAdresEmail);
		
		JFormattedTextField frmtdtxtfldKl = new JFormattedTextField();
		frmtdtxtfldKl.setText("KL01\\02\\14\\001");
		frmtdtxtfldKl.setBounds(192, 65, 147, 20);
		//frmtdtxtfldKl.setFormatterFactory(new DefaultFormatterFactory(createFormatter("KU ##/##/##/###",'_')));
		//frmtdtxtfldKl.setFocusLostBehavior(JFormattedTextField.COMMIT);
		panel.add(frmtdtxtfldKl);
		
		JFormattedTextField frmtdtxtfldMdrzecki = new JFormattedTextField();
		frmtdtxtfldMdrzecki.setText("Mądrzecki");
		frmtdtxtfldMdrzecki.setBounds(192, 96, 147, 20);
		panel.add(frmtdtxtfldMdrzecki);
		
		JFormattedTextField frmtdtxtfldMateusz = new JFormattedTextField();
		frmtdtxtfldMateusz.setText("Mateusz");
		frmtdtxtfldMateusz.setBounds(192, 127, 147, 20);
		panel.add(frmtdtxtfldMateusz);
		//# - Any valid number, uses Character.isDigit.
        //' - Escape character, used to escape any of the special formatting characters.
        //U - Any character (Character.isLetter). All lowercase letters are mapped to upper case.
        //L - Any character (Character.isLetter). All upper case letters are mapped to lower case.
        //A - Any character or number (Character.isLetter or Character.isDigit)
        //? - Any character (Character.isLetter).
        //* - Anything.
        //H - Any hex character (0-9, a-f or A-F).
		JFormattedTextField formattedTextField_3 = new JFormattedTextField();
		formattedTextField_3.setText("91120504414");
		formattedTextField_3.setBounds(192, 158, 147, 20);
		//formattedTextField_3.setFormatterFactory(new DefaultFormatterFactory(createFormatter("###########",'_')));
		//formattedTextField_3.setFocusLostBehavior(JFormattedTextField.COMMIT);
		panel.add(formattedTextField_3);
		
		JFormattedTextField frmtdtxtfldUllniana = new JFormattedTextField();
		frmtdtxtfldUllniana.setText("ul.Lniana 45 75-100 Koszalin");
		frmtdtxtfldUllniana.setBounds(192, 189, 147, 20);
		panel.add(frmtdtxtfldUllniana);
		
		JFormattedTextField formattedTextField_5 = new JFormattedTextField();
		formattedTextField_5.setText("+48 502-250-569");
		formattedTextField_5.setToolTipText("");
		formattedTextField_5.setBounds(192, 220, 147, 20);
		//formattedTextField_5.setFormatterFactory(new DefaultFormatterFactory(createFormatter("+48###-###-###",'_')));
		//formattedTextField_5.setFocusLostBehavior(JFormattedTextField.COMMIT);
		panel.add(formattedTextField_5);
		
		JFormattedTextField frmtdtxtfldMateuszmadrzeckipl = new JFormattedTextField();
		frmtdtxtfldMateuszmadrzeckipl.setText("mateusz@madrzecki.pl");
		frmtdtxtfldMateuszmadrzeckipl.setBounds(192, 251, 147, 20);
		panel.add(frmtdtxtfldMateuszmadrzeckipl);
	}
	public MaskFormatter createFormatter(String s, Character placeholder) {
	    MaskFormatter formatter = null;
	    try {
	        formatter = new MaskFormatter(s);
	        formatter.setPlaceholderCharacter(placeholder);
	       
	    } catch (java.text.ParseException exc) {
	    	
	      //  System.err.println("formatter is bad: " + exc.getMessage());
	       //System.exit(-1);
	    }
	    return formatter;
	}
}